<?php

namespace Drupal\vuelist\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a 'vuelist' Block.
 */
#[Block(
  id: "vuelist_block",
)]
class HelloBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
        '#theme' => 'module_name_block',
        '#attached' => [
          'library' => 'module_name/module-name-app',
        ],
      ];
  }

}
