import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

let wrapper = window.document.querySelector('.vuelist-wrapper')
if (wrapper) {
  let app = window.document.createElement('div')
  app.setAttribute('id', 'vuelist-app')
  wrapper.insertBefore(app, wrapper.childNodes[0])

  new Vue({
    render: h => h(App)
  }).$mount('#vuelist-app')
}